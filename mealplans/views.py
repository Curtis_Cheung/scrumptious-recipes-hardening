from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from mealplans.models import Mealplan
from mealplans.forms import RatingForm


class MealplansCreateView(LoginRequiredMixin, CreateView):
    model = Mealplan
    template_name = "mealplans_create.html"
    fields = ["name", "owner", "date", "recipes"]
    success_url = reverse_lazy("mealplans_list")


class MealplansEditView(UpdateView):
    model = Mealplan
    template_name = "mealplans_edit.html"
    fields = ["name", "owner", "date", "recipes"]
    success_url = reverse_lazy("mealplans_list")


class MealplansDeleteView(DeleteView):
    model = Mealplan
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplans_list")


class MealplansDetailView(DetailView):
    model = Mealplan
    template_name = "mealplans/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class MealplansListView(ListView):
    model = Mealplan
    template = "mealplans/list.html"
    paginate_by = 2