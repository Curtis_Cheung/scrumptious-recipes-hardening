from django.db import models
from django.conf import settings
USER_MODEL = settings.AUTH_USER_MODEL


class Mealplan(models.model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_names="mealplans",
        on_delete=models.CASCADE,
        null=True,)

    recipes = models.ManyToManyField("mealplan.Mealplan", related_named="mealplan")

    def __str__(self):
        return str(self.MealPlan)
