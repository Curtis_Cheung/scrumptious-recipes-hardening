from django.urls import path
from mealplans.views import (
    mealplansCreateView,
    mealplansListView,
    mealplansDeleteView,
    mealplansDetailView,
    mealplansEditView,
)
urlpatterns = [
    path("mealplans/", mealplansListView.as_view(), name="mealplans_list"),
    path("mealplans/create", mealplansCreateView.as_view(), name="mealplans_create"),
    path("mealplans/<int:pk>/", mealplansDetailView.as_view(), name="mealplans_detail"),
    path("mealplans/<int:pk>/edit", mealplansEditView.as_view(), name="mealplans_edit"),
    path("mealplans/<int:pk>/delete", mealplansDeleteView.as_view(), name="mealplans_delete"),
    ]
