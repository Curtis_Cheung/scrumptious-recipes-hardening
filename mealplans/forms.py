from django import forms


try:
    from mealplans.models import Mealplan

    class MealplansForm(forms.ModelForm):
        class Meta:
            model = Mealplan
            fields = [
                "name",
                "owner",
                "recipe",
                "date",
            ]

except Exception:
    pass


from mealplans.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
